const minilog = require('minilog');
minilog.enable();

module.exports = minilog('happycode-render');
